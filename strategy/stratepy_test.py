# -*- coding:utf-8 -*-
"""
简单卖平策略，仅做演示使用

Author: Qiaoxiaofeng
Date:   2020/09/10
Email: andyjoe318@gmail.com
"""
# 策略实现
import time
import cmath
import json
import redis
from requests import request
from alpha import const
from alpha.utils import tools
from alpha.utils import logger
from alpha.config import config
from alpha.market import Market
from alpha.trade import Trade
from alpha.order import Order
from alpha.orderbook import Orderbook
from alpha.kline import Kline
from alpha.markettrade import Trade as MarketTrade
from alpha.asset import Asset
from alpha.position import Position
from alpha.error import Error
from alpha.tasks import LoopRunTask
from alpha.order import ORDER_ACTION_SELL, ORDER_ACTION_BUY, ORDER_STATUS_FAILED, ORDER_STATUS_CANCELED, ORDER_STATUS_FILLED,\
    ORDER_TYPE_LIMIT, ORDER_TYPE_MARKET
from alpha.platforms.huobi_usdt_swap_api import HuobiUsdtSwapRestAPI


class MyStrategy:

    def __init__(self):
        """ 初始化
        """
        self.strategy = config.strategy
        self.platform = config.accounts[0]["platform"]
        self.account = config.accounts[0]["account"]
        self.access_key = config.accounts[0]["access_key"]
        self.secret_key = config.accounts[0]["secret_key"]
        self.host = config.accounts[0]["host"]
        self.wss = config.accounts[0]["wss"]
        self.symbol = config.symbol
        self.contract_type = config.contract_type
        self.channels = config.markets[0]["channels"]
        self.orderbook_length = config.markets[0]["orderbook_length"]
        self.orderbooks_length = config.markets[0]["orderbooks_length"]
        self.klines_length = config.markets[0]["klines_length"]
        self.trades_length = config.markets[0]["trades_length"]
        self.market_wss = config.markets[0]["wss"]
        self.api = HuobiUsdtSwapRestAPI("https://api.hbdm.vn","access key","secret key")
        self.r = redis.Redis(host='0.0.0.0', port=6379)
        # 杠杆与面值
        self.lever_rate = 1
        self.contract_size = 0.001
        self.orderbook_invalid_seconds = 10

        self.last_bid_price = 0 # 上次的买入价格
        self.last_ask_price = 0 # 上次的卖出价格
        self.last_orderbook_timestamp = 0 # 上次的orderbook时间戳

        self.raw_symbol = self.symbol.split('-')[0]

        self.ask1_price = 0
        self.bid1_price = 0
        self.ask1_volume = 0
        self.bid1_volume = 0


        # # 交易模块
        # cc = {
        #     "strategy": self.strategy,
        #     "platform": self.platform,
        #     "symbol": self.symbol,
        #     "contract_type": self.contract_type,
        #     "account": self.account,
        #     "access_key": self.access_key,
        #     "secret_key": self.secret_key,
        #     "host": self.host,
        #     "wss": self.wss,
        #     "order_update_callback": self.on_event_order_update,
        #     "asset_update_callback": self.on_event_asset_update,
        #     "position_update_callback": self.on_event_position_update,
        #     "init_success_callback": self.on_event_init_success_callback,
        # }
        # self.trader = Trade(**cc)

        # 行情模块
        cc = {
            "platform": self.platform,
            "symbols": [self.symbol],
            "channels": self.channels,
            "orderbook_length": self.orderbook_length,
            "orderbooks_length": self.orderbooks_length,
            "klines_length": self.klines_length,
            "trades_length": self.trades_length,
            "wss": self.market_wss,
            # "orderbook_update_callback": self.on_event_orderbook_update,
            # "kline_update_callback": self.on_event_kline_update,
            # "trade_update_callback": self.on_event_trade_update
        }
        self.market = Market(**cc)
                
        # 60秒执行1次
        LoopRunTask.register(self.get_boll, 1)
    async def notice_by_boll_middle(self):
        """ 布林带中部趋势监控
        """
        data = await self.get_boll()
        DN = data['DN']
        UP = data['UP']
        MB = data['MB']
        currentPrice = data['currentPrice']
        current_price_target = "upper" if currentPrice>= MB else "low"
        if int(self.r.get('huobi_currentPrice'))>
        self.r.set('huobi_currentPrice')
        
    async def notice_by_boll(self,*args,**kwargs):
        #布林带顶峰谷底监控
        data = await self.get_boll()
        DN = data['DN']
        UP = data['UP']
        MB = data['MB']
        currentPrice = data['currentPrice']
        print("MB:",round(MB,5),"UP:",round(UP.real,5),"DN:",round(DN.real,5),"当前价:",currentPrice)
        if (currentPrice>round(UP.real,5) or currentPrice<round(DN.real,5)) and not self.r.get('send_message'):
            if not self.r.get('send_message'):
                if (currentPrice>round(UP.real,5)):
                    if self.r.get('huobi_count') and self.r.get('huobi_type')=='up':
                        self.r.set('huobi_count',int(self.r.get('huobi_count'))+1)
                    else:
                        self.r.set('huobi_count',1)
                        self.r.set('huobi_type','up')
                elif currentPrice<round(DN.real,5):
                    if self.r.get('huobi_count') and self.r.get('huobi_type')=='down':
                        self.r.set('huobi_count',int(self.r.get('huobi_count'))+1)
                    else:
                        self.r.set('huobi_count',1)
                        self.r.set('huobi_type','down')      
                content = '{symbol}-{price}-{count}-{type}'.format(symbol= self.symbol,price=currentPrice,count=self.r.get('huobi_count'),type=self.r.get('huobi_type'))    
                message =     {
                   "touser" : "6003",
                   "toparty" : "",
                   "totag" : "",
                   "msgtype" : "text",
                   "agentid" : 1000040,
                   "text" : {
                       "content" : content
                   },
                   "safe":0,
                   "enable_id_trans": 0,
                   "enable_duplicate_check": 0,
                   "duplicate_check_interval": 1800
                }
                self.send_notice(message)
                self.r.set('send_message',"1",px=300000)
            else:
                print("消息间隔中。。。")

            
    
    def get_access_token(self,Secret,token_name=None):
        Secret = Secret
        if self.r.get('doge-access_token'):
            access_token = self.r.get('doge-access_token')
        else:
            url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={corpid}&corpsecret={corpsecret}".format(corpid="ww496415a282ed8602",corpsecret=Secret)
            response = request('GET',url,allow_redirects=False)
            access_token = response.json()['access_token']
            self.r.set('doge-access_token',access_token,px=900000)
        return access_token
            
    def send_notice(self,message):
        url = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token={ACCESS_TOKEN}".format(ACCESS_TOKEN=self.get_access_token("j36KGEc1xnrz05xahrPvBh231JqKm0zCe3eYjMYcEP8"))
        request('POST',url,data=json.dumps(message))

    async def get_boll(self):
        #获取布林带相关数据
        total = 0
        total_sum = 0
        data = await self.api.get_klines(contract_code="CHR-USDT",period="5min",sfrom=int(time.time())-(60*95),to=int(time.time()))
        for item in data[0]['data']:
            total+=float(item['close'])
        MB = total/20
        for item in data[0]['data']:
            total_sum += (float(item['close'])-MB)**2
        MD = 2*cmath.sqrt(total_sum/20)
        UP = MB+MD
        DN = MB-MD
        currentPrice = data[0]['data'][-1]['close']
        return {
            "MB":MB,
            "UP":UP,
            "DN":DN,
            "currentPrice":currentPrice
        }